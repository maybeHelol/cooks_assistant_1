package CookSAssistant;

import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

public enum Places {
    cooking(
            Area.polygonal(
                    new Position[] {
                            new Position(3159, 3294, 0),
                            new Position(3161, 3288, 0),
                            new Position(3168, 3291, 0)
                    }
            )),
    mill(
            Area.polygonal(
                    new Position[] {
                            new Position(3166, 3311, 0),
                            new Position(3161, 3306, 0),
                            new Position(3168, 3301, 0),
                            new Position(3172, 3309, 0)
                    }
            )),
    mill1st(
            Area.polygonal(
            new Position[] {
        new Position(3166, 3311, 1),
                new Position(3161, 3306, 1),
                new Position(3168, 3301, 1),
                new Position(3172, 3309, 1)
    }
        )),
    mill2nd(
            Area.polygonal(
            new Position[] {
        new Position(3166, 3311, 2),
                new Position(3161, 3306, 2),
                new Position(3168, 3301, 2),
                new Position(3172, 3309, 2)
    }

            )),
    goingformilk(Area.polygonal(
            new Position[] {
                    new Position(3254, 3278, 0),
                    new Position(3251, 3274, 0),
                    new Position(3255, 3270, 0),
                    new Position(3258, 3276, 0)
            }
    )
    ),
    goingforbucket(Area.polygonal(new Position[]{
            new Position(3225, 3295, 0),
            new Position(3231, 3295, 0),
            new Position(3231, 3287, 0),
            new Position(3225, 3289, 0)
    })),
    goingforegg(Area.polygonal(
            new Position[] {
                    new Position(3227, 3301, 0),
                    new Position(3225, 3295, 0),
                    new Position(3235, 3295, 0),
                    new Position(3236, 3302, 0)
})),
    goingforcook1(Area.polygonal(
            new Position[] {
                    new Position(3205, 3218, 0),
                    new Position(3213, 3218, 0),
                    new Position(3213, 3209, 0),
                    new Position(3205, 3210, 0)
            }

    )),
    goingforcookBetween(Area.polygonal(new Position[] {
            new Position(3198, 3242, 0),
            new Position(3202, 3236, 0),
            new Position(3196, 3226, 0),
            new Position(3191, 3242, 0)
    }));




    private Area area;


    Places(Area area) {
        this.area = area;
    }
    public Area getArea(){
        return area;
    }
}
