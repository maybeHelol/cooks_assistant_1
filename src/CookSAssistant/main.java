package CookSAssistant;

import CookSAssistant.Places;

import jdk.nashorn.internal.ir.IfNode;
import org.rspeer.networking.dax.walker.models.WalkState;
import org.rspeer.runetek.adapter.Interactable;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;

import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.input.menu.tree.WalkAction;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.*;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

import java.util.Random;

@ScriptMeta(developer="Gnoj", name="Cook's Assistant's - SimpleQuester", desc = "Does Cook's Assistant fast no requirements.")
public class main extends Script {
public static Places place1 = Places.cooking;
public static Places windmill = Places.mill;
public static Places floor2mill = Places.mill2nd;
public static Places cow = Places.goingformilk;
private static Places bucket = Places.goingforbucket;
private static Places egg = Places.goingforegg;
public static Places cook1 = Places.goingforcook1;
private static Places cook2 = Places.goingforcookBetween;
    @Override
    public void onStart() {   }

    @Override
    public int loop() {
        while(!cook1.getArea().contains(Players.getLocal().getPosition())){
            Movement.walkToRandomized(cook1.getArea().getCenter());
            Time.sleep(3500,3899);
        }
        Npc cook = Npcs.getNearest("Cook");
        cook.interact("Talk-to");
        Time.sleep(2500,3700);
        if(Inventory.contains("Pot of flour", "Egg", "Bucket of milk")){
            cook.interact("Talk-to");

            while(Dialog.isOpen()) {
                Dialog.process(0);

                Time.sleep(700, 980);
            }
            setStopping(true);
        }

        if(Dialog.isOpen()) {
            Dialog.processContinue();

            Time.sleep(700, 980);
            Log.fine("talking");
        }
        if(!Inventory.contains("Pot")){
            Pickable pot = Pickables.getNearest("Pot");
            pot.interact("Take");
            Time.sleep(2200, 2333);
        }



        while(!place1.getArea().contains(Players.getLocal().getPosition())){
            if (Movement.getRunEnergy() >= 7 && !Movement.isRunEnabled()){
                Movement.toggleRun(true);
            }
            Movement.walkTo(place1.getArea().getCenter());
            Time.sleep(3000, 4200);
        }

        Time.sleep(400);
        SceneObject wheat = SceneObjects.getNearest("Wheat");
        wheat.interact("Pick");
        Time.sleep(1950, 2000);
        Inventory.getFirst("Wheat");
        Log.fine("Getting to windmill");
        while(!windmill.getArea().contains(Players.getLocal().getPosition())){
            Movement.walkToRandomized(windmill.getArea().getCenter());
            if (Movement.getRunEnergy() > 7 && !Movement.isRunEnabled()){
                Movement.toggleRun(true);
            }
            Time.sleep(2500,2990);
        }
        while(!floor2mill.getArea().contains(Players.getLocal().getPosition())) {
            Movement.walkToRandomized(floor2mill.getArea().getCenter());
            if (Movement.getRunEnergy() > 7 && !Movement.isRunEnabled()){
                Movement.toggleRun(true);
            }
            Time.sleep(2700,3200);
        }

        SceneObject hopper = SceneObjects.getNearest("Hopper");
        hopper.interact("Fill");
        Time.sleep(3500,4000);
        SceneObject hc = SceneObjects.getNearest("Hopper controls");
        hc.interact("Operate");
        Time.sleep(3200,3500);

        while(!windmill.getArea().contains(Players.getLocal().getPosition())){
            Movement.walkToRandomized(windmill.getArea().getCenter());
        }

        SceneObject fb = SceneObjects.getNearest("Flour bin");
        fb.interact("Empty");
        Time.sleep(3000,4000);

        while(!egg.getArea().contains(Players.getLocal().getPosition())){
            Movement.walkToRandomized(egg.getArea().getCenter());
            if (Movement.getRunEnergy() >= 7 && !Movement.isRunEnabled()){
                Movement.toggleRun(true);
            }
            Time.sleep(3500,3777);
        }

        Pickable egg = Pickables.getNearest("Egg");
        egg.interact("Take");
        Time.sleep(2300,3310);

        while(!bucket.getArea().contains(Players.getLocal().getPosition())){
            Movement.walkToRandomized(bucket.getArea().getCenter());
            if (Movement.getRunEnergy() >= 7 && !Movement.isRunEnabled()){
                Movement.toggleRun(true);
            }
            Time.sleep(3200);
        }
        Pickable bucket = Pickables.getNearest("Bucket");
        bucket.interact("Take");
        Time.sleep(2980,3500);

        while(!cow.getArea().contains(Players.getLocal().getPosition())){
            Movement.walkToRandomized(cow.getArea().getCenter());
            if (Movement.getRunEnergy() > 7 && !Movement.isRunEnabled()){
                Movement.toggleRun(true);
            }
            Time.sleep(3000,3900);
        }
        Time.sleep(2500);
        SceneObject milkingcow = SceneObjects.getNearest("Dairy cow");
        milkingcow.interact("Milk");
        Time.sleep(5800, 6300);
        while(!cook2.getArea().contains(Players.getLocal().getPosition())){
            Movement.walkToRandomized(cook2.getArea().getCenter());
            if (Movement.getRunEnergy() > 7 && !Movement.isRunEnabled()){
                Movement.toggleRun(true);
            }
            Time.sleep(2900,3500);
        }

        while(!cook1.getArea().contains(Players.getLocal().getPosition())){
            Movement.walkTo(cook1.getArea().getCenter());
            if (Movement.getRunEnergy() > 7 && !Movement.isRunEnabled()){
                Movement.toggleRun(true);
            }
            Time.sleep(2700,3500);
        }
        Npc cook1 = Npcs.getNearest("Cook");
        cook1.interact("Talk-to");
        Time.sleep(2800, 3200);
        while(Dialog.isOpen()){
            Dialog.process(0);

            Time.sleep(800, 980);
        }
        setStopping(true);



        return 1230;
    }

    public static int randInt(int min, int max){
        Random rand = null;
        int randomNum = rand.nextInt((max-min)+1)+min;
        return randomNum;
    }
}

